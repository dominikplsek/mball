# mball

mBall is a simple iOS game based on an original [Neverball](http://neverball.org) programmed using a Metal API. The purpose of this project is to learn essential graphics programming and explore Metal API.


## Credits
- A ball OBJ model and texture is taken from [Neverball repository](https://github.com/Neverball/neverball/tree/master/data/ball/basic-ball)
- [Trap OBJ model and texture](http://tf3dm.com/3d-model/trap-65059.html)
- [Coin OBJ model](https://github.com/Neverball/neverball/blob/master/data/item/coin/coin.obj)
- [Dogecoin texture](http://www.turbosquid.com/3d-models/free-max-mode-doge-coin/787687)

We have used these sources to learn more about Metal:
- [Metal By Example from Warren Moore](http://metalbyexample.com)
- [Apple's guides and sample codes](https://developer.apple.com/metal/)
- WWDC 14 and 15 videos about Metal
- [Ray Wenderlich iOS 8 Metal Tutorial series](https://www.raywenderlich.com/77488/ios-8-metal-tutorial-swift-getting-started)
