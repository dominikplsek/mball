//
//  EntityManager.swift
//  mBall
//
//  Created by Dominik Plšek on 02/10/2016.
//  Copyright © 2016 mBall Team. All rights reserved.
//

import Foundation

class EntityManager {
    
    typealias EntityID = Int
    typealias ComponentClassName = String
    
    private var entities: [EntityID]
    private var componentsByClass: [ComponentClassName: [EntityID: Component]]
    private var lowestUnassignedEntityID: Int
    
    public static var shared: EntityManager!
    
    public init() {
        entities = []
        componentsByClass = [:]
        lowestUnassignedEntityID = 1
    }
    
    func generateNewEntityID() -> Int? {
        if (lowestUnassignedEntityID < Int.max) {
            return lowestUnassignedEntityID
        }
        else {
            for uniqueID in 1..<Int.max  {
                if !entities.contains(uniqueID) {
                    return uniqueID
                }
            }
            return nil
        }
    }
    
    func createEntity() -> Entity? {
        guard let uniqueEntityID = generateNewEntityID() else { return nil }
        entities.append(uniqueEntityID)
        
        return Entity(uniqueID: uniqueEntityID)
    }
    
    func removeEntity(entity: Entity) {
        for var components in componentsByClass.values {
            if components[entity.uniqueID] != nil {
                components.removeValue(forKey: entity.uniqueID)
            }
        }
        entities.remove(at: entity.uniqueID)
    }
    
    func entities<T: Component>(forComponent component: T.Type) -> [Entity] {
        guard let components = componentsByClass[String(describing: component)] else { return [] }
        
        var entities = [Entity]()
        
        for entityID in components.keys {
            entities.append(Entity(uniqueID: entityID))
        }
        
        return entities
    }
    
    func addComponent<T: Component>(component: T, toEntity entity: Entity) {
        if var components = componentsByClass[String(describing: type(of: component))] {
            // Components of passed Component class already exists
            components[entity.uniqueID] = component
        } else {
            // Create new key-value  pair
            componentsByClass[String(describing: type(of: component))] = [entity.uniqueID: component]
        }
    }
    
    func component<T: Component>(forClass aClass: T.Type, entity: Entity) -> Component? {
        return componentsByClass[String(describing: aClass)]?[entity.uniqueID]
    }
}
