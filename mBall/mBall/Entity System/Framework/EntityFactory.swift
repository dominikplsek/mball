//
//  EntityFactory.swift
//  mBall
//
//  Created by Dominik Plšek on 03/10/2016.
//  Copyright © 2016 mBall Team. All rights reserved.
//

import Foundation

class EntityFactory {
    
    private var entityManager: EntityManager
        
    init(entityManager: EntityManager) {
        self.entityManager = entityManager
    }
    
    func createPlayer() -> Entity? {
        guard let playerEntity = entityManager.createEntity() else { return nil }
        
        
        return playerEntity
    }
}
