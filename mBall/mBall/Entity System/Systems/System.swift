//
//  System.swift
//  mBall
//
//  Created by Dominik Plšek on 02/10/2016.
//  Copyright © 2016 mBall Team. All rights reserved.
//

import Foundation

protocol System {
    var entityManager: EntityManager { get set }
    func update(dt: Float) -> Void
}
