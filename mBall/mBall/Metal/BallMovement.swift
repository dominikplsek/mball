//
//  BallMovement.swift
//  mBall
//
//  Created by Jiri Skotak on 27.11.16.
//  Copyright © 2016 mBall Team. All rights reserved.
//

import Foundation
import CoreMotion
import CoreGraphics

let MaxBallAcceleration: Double = 0.2
let MaxBallSpeed: Double = 0.1

class BallMovement {
    
    var accelerometerX: Double = 0
    var accelerometerY: Double = 0
    var ballAcceleration = CGVector(dx: 0, dy: 0)
    let motionManager = CMMotionManager()
    var first: Bool = true
    var firstAccelerationX: Double = 0
    var firstAccelerationY: Double = 0
    
    init() {
        startMonitoringAcceleration()
    }
    deinit {
        stopMonitoringAcceleration()
    }
    func startMonitoringAcceleration() {
        
        if motionManager.isAccelerometerAvailable {
            motionManager.startAccelerometerUpdates()
            NSLog("accelerometer updates on...")
        }
    }
    
    func stopMonitoringAcceleration() {
        
        if motionManager.isAccelerometerAvailable && motionManager.isAccelerometerActive {
            motionManager.stopAccelerometerUpdates()
            NSLog("accelerometer updates off...")
        }
    }
    func updateBallAccelerationFromMotionManager() -> CGVector{

        if let acceleration = motionManager.accelerometerData?.acceleration {
            if first {
                firstAccelerationX = acceleration.x
                firstAccelerationY = acceleration.y
                accelerometerX = 0
                accelerometerY = 0
                first = false
            }
            else {
                let filterFactor: Double = 0.75
                let reduction: Double = 20.0
                
                accelerometerX = ((acceleration.x-firstAccelerationX) * filterFactor + accelerometerX * (1 - filterFactor))/reduction
                accelerometerY = ((acceleration.y-firstAccelerationY) * filterFactor + accelerometerY * (1 - filterFactor))/reduction
            }
        }
        else {
            
            accelerometerX = 0
            accelerometerY = 0
        }
        
        return CGVector(dx:accelerometerX, dy:accelerometerY)
    }
}
