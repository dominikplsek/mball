//
//  Mesh.swift
//  mBall
//
//  Created by Dominik Plšek on 03/10/2016.
//  Copyright © 2016 mBall Team. All rights reserved.
//

import Foundation
import Metal
import QuartzCore
import simd

import Foundation
import MetalKit
import QuartzCore
import simd

class Mesh {   
    var mesh: MTKMesh! = nil
    var submeshes: [Submesh] = []
    
    init(mtkMesh: MTKMesh, mdlMesh: MDLMesh, device: MTLDevice) {
        self.mesh = mtkMesh
        
        for index in 0..<mtkMesh.submeshes.count {
            let submesh = Submesh(submesh: mtkMesh.submeshes[index],
                                  mdlSubmesh: mdlMesh.submeshes?[index] as? MDLSubmesh,
                                  device: device)
            submeshes.append(submesh)
        }
    }
    
    func render(encoder: MTLRenderCommandEncoder) {
        var bufferIndex = 0
        
        for vertexBuffer in mesh.vertexBuffers {
            encoder.setVertexBuffer(vertexBuffer.buffer, offset: vertexBuffer.offset, at: bufferIndex)
            bufferIndex += 1
        }
        
        for submesh in submeshes {
            submesh.render(encoder: encoder)
        }
    }
}
