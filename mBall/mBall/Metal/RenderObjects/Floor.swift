//
//  Floor.swift
//  mBall
//
//  Created by Jiri Skotak on 30.11.16.
//  Copyright © 2016 mBall Team. All rights reserved.
//

import MetalKit

class Floor: RenderObject {
    
    override init(name: String, meshes: [Mesh]) {
        
        for mesh in meshes {
            for submesh in mesh.submeshes {
                let materialUniformsPtr = UnsafeMutablePointer<MaterialUniforms>(OpaquePointer(submesh.materialUniformsBuffer.contents()))
                var materialUniforms = materialUniformsPtr.pointee
                materialUniforms.ambientColor = float4(0.4, 0.4, 0.4, 1.0)
                materialUniforms.diffuseColor = float4(0.2, 0.2, 0.2, 1.0)
                materialUniformsPtr.pointee = materialUniforms
            }
        }
        super.init(name: name, meshes: meshes)
        self.scale = floorSize
        self.positionY = -0.09
    }
}
