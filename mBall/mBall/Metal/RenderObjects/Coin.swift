//
//  Coin.swift
//  mBall
//
//  Created by Dominik Plšek on 30/11/2016.
//  Copyright © 2016 mBall Team. All rights reserved.
//

import MetalKit

class Coin: RenderObject {
    override init(name: String, meshes: [Mesh]) {
        for mesh in meshes {
            for submesh in mesh.submeshes {
                let materialUniformsPtr = UnsafeMutablePointer<MaterialUniforms>(OpaquePointer(submesh.materialUniformsBuffer.contents()))
                var materialUniforms = materialUniformsPtr.pointee
                materialUniforms.ambientColor = float4(0.4, 0.4, 0.4, 1.0)
                materialUniforms.diffuseColor = float4(0.6, 0.6, 0.6, 1.0)
                materialUniformsPtr.pointee = materialUniforms
            }
        }
        super.init(name: name, meshes: meshes)
        positionX = generateRandomPosition()
        positionZ = generateRandomPosition()
        scale = 0.06
    }
    override func buildModelMatrix() -> float4x4 {
        
        var matrix = float4x4()
        
        rotationY -= 0.01
    
        matrix.translate(positionX, y: positionY, z: positionZ)
        matrix.rotateAroundX(0.0, y: rotationY, z: 0.0)
        matrix.scale(scale, y: scale, z: scale)
        return matrix
    }
}


extension Coin {
    func generateRandomPosition() -> Float {
        return (Float(arc4random()) / Float(UINT32_MAX))*(floorSize - 0.2)
    }
}
