//
//  Ball.swift
//  mBall
//
//  Created by Jiri Skotak on 27.11.16.
//  Copyright © 2016 mBall Team. All rights reserved.
//

import MetalKit

class Ball: RenderObject {
    
    var ballMoveSpeed: Float = 0.0
    var ballDirection: Float = 0.0
    let rotationBooster: Float = 11.0
    let ballMovement: BallMovement
    
    override init(name: String, meshes: [Mesh]) {
        
        for mesh in meshes {
            for submesh in mesh.submeshes {
                let materialUniformsPtr = UnsafeMutablePointer<MaterialUniforms>(OpaquePointer(submesh.materialUniformsBuffer.contents()))
                var materialUniforms = materialUniformsPtr.pointee
                materialUniforms.ambientColor = float4(0.6, 0.6, 0.6, 1.0)
                materialUniforms.diffuseColor = float4(0.8, 0.8, 0.8, 1.0)
                materialUniformsPtr.pointee = materialUniforms
             }
        }

        ballMovement = BallMovement()

        super.init(name: name, meshes: meshes)
        
        scale = 0.07
        positionZ = 3.35
    }
    
    override func updateLogic() {
        let movement = ballMovement.updateBallAccelerationFromMotionManager()
        ballMoveSpeed = Float(movement.dx)
        ballMoveSpeed = Float(movement.dx)
        
        ballDirection -= Float(movement.dy)
        ballDirection -= Float(movement.dy)
    }
    
    override func buildModelMatrix() -> float4x4 {
        
        var matrix = float4x4()
        
        rotationX -= rotationBooster*self.ballMoveSpeed
        rotationY = -self.ballDirection
        
        let movementX = sin(self.ballDirection)*self.ballMoveSpeed
        let movementZ = cos(self.ballDirection)*self.ballMoveSpeed
        
        
        if(positionX-movementX < (floorSize-0.1) && positionX-movementX > -(floorSize-0.1) &&
           positionZ-movementZ < (floorSize-0.1) && positionZ-movementZ > -(floorSize-0.1))
        {
            positionX -= movementX
            positionZ -= movementZ
        }
        
        matrix.translate(-positionX, y: positionY, z: positionZ)
        matrix.rotateAroundX(0.0, y: -self.ballDirection, z: 0.0)
        matrix.rotateAroundX(rotationX, y: 0.0, z: 0.0)
        matrix.scale(scale, y: scale, z: scale)
        
        return matrix
    }
    
}
