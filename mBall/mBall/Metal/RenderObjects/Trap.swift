//
//  Trap.swift
//  mBall
//
//  Created by Dominik Plšek on 01/12/2016.
//  Copyright © 2016 mBall Team. All rights reserved.
//

import MetalKit

class Trap: RenderObject {
    
    override init(name: String, meshes: [Mesh]) {
        for mesh in meshes {
            for submesh in mesh.submeshes {
                let materialUniformsPtr = UnsafeMutablePointer<MaterialUniforms>(OpaquePointer(submesh.materialUniformsBuffer.contents()))
                var materialUniforms = materialUniformsPtr.pointee
                materialUniforms.ambientColor = float4(0.2, 0.2, 0.2, 1.0)
                materialUniforms.diffuseColor = float4(0.8, 0.8, 0.8, 1.0)
                materialUniformsPtr.pointee = materialUniforms
            }
        }
        
        super.init(name: name, meshes: meshes)
        positionX = generateRandomPosition()
        positionY = -0.09
        positionZ = generateRandomPosition()
        scale = 0.01
    }
}

extension Trap {
    func generateRandomPosition() -> Float {
        return (Float(arc4random()) / Float(UINT32_MAX))*(floorSize - 0.2)
    }
}
