//
//  ShaderTypes.swift
//  mBall
//
//  Created by Dominik Plšek on 24/11/2016.
//

import simd

/// Indices of vertex attribute in descriptor.
enum VertexAttributes {
    case VertexAttributePosition
    case VertexAttributeNormal
    case VertexAttributeTexcoord
}

/// Indices for texture bind points.
enum TextureIndex {
    case DiffuseTextureIndex
}

/// Indices for buffer bind points.
enum BufferIndex {
    case MeshVertexBuffer
    case UniformBuffer
    case MaterialUniformBuffer
}

/// Shader uniforms.
struct Uniforms {
    var modelMatrix: float4x4
    var modelViewMatrix: float4x4
    var normalMatrix: float4x4
    var modelViewProjectionMatrix: float4x4
}

/// Material uniforms.
struct MaterialUniforms {
    var emissiveColor: float4
    var ambientColor: float4
    var diffuseColor: float4
    var specularColor: float4
    var specularIntensity: Float
    
    var pad1: Float;
    var pad2: Float;
    var pad3: Float;
}
