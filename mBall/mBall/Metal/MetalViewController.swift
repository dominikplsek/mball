//
//  MetalViewController.swift
//  mBall
//
//  Created by Dominik Plšek on 24/10/2016.
//  Copyright © 2016 mBall Team. All rights reserved.
//

import Foundation

import UIKit
import MetalKit


class MetalViewController: UIViewController {
    
    fileprivate var renderer: Renderer!
    fileprivate var game: Game!
    var timer: Timer!
    
    fileprivate var playerLose = false
    
    override func viewWillAppear(_ animated: Bool) {
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(updateGameTime), userInfo: nil, repeats: true)
        
        game = Game()
        Game.shared = game
        game.delegate = self
        game.allCoinsCollected = self.allCoinsColleced
        renderer = Renderer(view: mtkView)
        
        setupLabels()
        
        super.viewWillAppear(animated)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        timer.invalidate()
        timer = nil
        super.viewWillDisappear(animated)
    }
    
    func updateGameTime() {
        game.time += 1
        timeLabel.text = "\(game.time)"
    }
    
    func setupLabels() {
        timeLabel.text = "\(game.time)"
        coinsLabel.text = "\(game.collectedCoins)"
    }
    
    // MARK: - IBOutlets
    @IBOutlet weak var mtkView: MTKView! {
        didSet {
            mtkView.delegate = self
            mtkView.preferredFramesPerSecond = 60
            mtkView.clearColor = MTLClearColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 1.0)
            mtkView.sampleCount = 4
            mtkView.depthStencilPixelFormat = MTLPixelFormat.depth32Float_stencil8
        }
    }
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var coinsLabel: UILabel!
}

extension MetalViewController: GameEvents {
    
    func allCoinsColleced()
    {
        if(game.collectedCoins == game.numberOfCoins)   {
            performSegue(withIdentifier: "GameFinishedSegue", sender: self)
        }
    }
    
    func playerDidActivateTrap() {
        playerLose = true
        performSegue(withIdentifier: "GameFinishedSegue", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "GameFinishedSegue"  {
            let newGameViewController: NewGameViewController = segue.destination as! NewGameViewController
            newGameViewController.inTime = game.time
            newGameViewController.coinsCollected = game.collectedCoins
            if playerLose {
                newGameViewController.playerLose = true
            }
        }
    }
}

extension MetalViewController: MTKViewDelegate {
    func mtkView(_ view: MTKView, drawableSizeWillChange size: CGSize) {
        renderer.reshape()
    }
    
    func draw(in view: MTKView) {
        renderer.render(view: view)
        coinsLabel.text = "\(game.collectedCoins)"
    }
}
