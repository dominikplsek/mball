//
//  Shaders.metal
//  mBall
//
//  Created by Dominik Plšek on 01/10/2016.
//  Copyright © 2016 mBall Team. All rights reserved.
//
#include <simd/simd.h>
#include <metal_texture>
#include <metal_matrix>
#include <metal_geometric>
#include <metal_math>
#include <metal_graphics>
#include <metal_stdlib>
using namespace simd;

enum VertexAttributes {
    VertexAttributePosition = 0,
    VertexAttributeNormal   = 1,
    VertexAttributeTexcoord = 2,
};
    
enum TextureIndex {
    DiffuseTextureIndex = 0
};

enum BufferIndex  {
    MeshVertexBuffer          = 0,
    UniformBuffer             = 1,
    MaterialUniformBuffer     = 2
};
    
struct Uniforms {
    float4x4 modelMatrix;
    float4x4 normalMatrix;
    float4x4 modelViewMatrix;
    float4x4 modelViewProjectionMatrix;
};

struct MaterialUniforms {
    float4 emissiveColor;
    float4 ambientColor;
    float4 diffuseColor;
    float4 specularColor;
    
    float specularIntensity;
    
    float pad1;
    float pad2;
    float pad3;
};
    
using namespace metal;

struct Light
{
    float3 direction;
    float4 ambientColor;
    float ambientIntensity;
    float4 diffuseColor;
    float4 specularColor;
};
    
// Variables in constant address space.
constant Light light = {
    .direction = float3(0.5, 0.72, 0.68),
    .ambientColor = float4(1.0, 1.0, 1.0, 1.0),
    .ambientIntensity = 0.3,
    .diffuseColor = float4(0.9, 0.9, 0.9, 1.0),
    .specularColor = float4(1.0, 1.0, 1.0, 1.0)
};
    
// Per-vertex input structure
struct Vertex {
    float3 position [[ attribute(VertexAttributePosition) ]];
    float3 normal   [[ attribute(VertexAttributeNormal) ]];
    float2 texcoord  [[ attribute(VertexAttributeTexcoord)]];
};

// Per-vertex output and per-fragment input
struct ProjectedVertex {
    float4 position [[ position ]];
    float2 texcoord;
    float4 positionW;
    float4 normal;
    float3 eye;
};
    
// Vertex shader function
vertex ProjectedVertex vertex_program(Vertex in [[ stage_in ]],
                                      constant Uniforms& uniforms [[ buffer(UniformBuffer) ]]) {
    ProjectedVertex outVert;

    float4 in_position = float4(in.position, 1.0);
   
    outVert.position = uniforms.modelViewProjectionMatrix * in_position;
    outVert.normal = uniforms.normalMatrix * float4(in.normal, 0.0);
    outVert.positionW = uniforms.modelMatrix * in_position;
    outVert.eye = -(uniforms.modelViewMatrix * in_position).xyz;
    outVert.texcoord = in.texcoord;

    return outVert;
}

// Fragment shader function
fragment half4 fragment_program(ProjectedVertex in [[stage_in]],
                                texture2d<half>  diffuseTexture [[ texture(DiffuseTextureIndex) ]],
                                constant MaterialUniforms& materialUniforms [[ buffer(0) ]]) {
    constexpr sampler defaultSampler;
    
    float3 normal = normalize(in.normal.xyz);
    float3 lightW = light.direction - in.positionW.xyz;
    
    half4 color = diffuseTexture.sample(defaultSampler, float2(in.texcoord));

    // Ambient
    half4 ambientColor = half4(light.ambientColor * materialUniforms.ambientColor);
    
    // Diffuse
    float diffuseFactor = max(0.0, dot(normal, lightW));

    //loat diffuseFactor = saturate(dot(normal, light.direction));
    half4 diffuseColor = half4(light.diffuseColor * diffuseFactor * materialUniforms.diffuseColor);

    // Specular
    half4 specularColor = 0.0;
    if (diffuseFactor > 0)
    {
        float3 eyeDirection = normalize(in.eye);
        float3 halfway = normalize(lightW + eyeDirection);
        float specularFactor = pow(saturate(dot(normal, halfway)), materialUniforms.specularIntensity);
        specularColor = half4(light.specularColor * specularFactor * materialUniforms.specularColor);
    }
    
    return color * (ambientColor + diffuseColor + specularColor);
}
