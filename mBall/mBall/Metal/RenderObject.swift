//
//  RenderObject.swift
//  mBall
//
//  Created by Dominik Plšek on 27/11/2016.
//  Copyright © 2016 mBall Team. All rights reserved.
//

import MetalKit

class RenderObject {
    var positionX: Float = 0.0
    var positionY: Float = 0.0
    var positionZ: Float = 0.0
    
    var rotationX: Float = 0.0
    var rotationY: Float = 0.0
    var rotationZ: Float = 0.0
    
    
    var scale: Float = 1.0
    
    var floorSize: Float = 3.5
    
    let meshes: [Mesh]
    
    var buffer: MTLBuffer?
    
    let name: String
    
    init(name: String, meshes: [Mesh]) {
        self.name = name
        self.meshes = meshes
    }
    
    func updateUniforms(viewMatrix: float4x4, projectionMatrix: float4x4) {
        let modelMatrix = buildModelMatrix()
        let modelViewMatrix = viewMatrix * modelMatrix
        let normalMatrix = modelViewMatrix.transpose.inverse
        let modelViewProjectionMatrix = projectionMatrix * modelViewMatrix
        
        buffer = BufferProvider.shared.nextUniformsBuffer(modelMatrix: modelMatrix,
                                                          modelViewMatrix: modelViewMatrix,
                                                          normalMatrix: normalMatrix,
                                                          modelViewProjectionMatrix: modelViewProjectionMatrix)
    }
    
    func updateLogic() {}
    
    func update(viewMatrix: float4x4, projectionMatrix: float4x4) {
        updateLogic()
        updateUniforms(viewMatrix: viewMatrix, projectionMatrix: projectionMatrix)
    }
    
    func render(encoder: MTLRenderCommandEncoder) {
        encoder.setVertexBuffer(buffer, offset: 0, at: BufferIndex.UniformBuffer.hashValue)
        
        for mesh in meshes {
            mesh.render(encoder: encoder)
        }
    }
    
    func buildModelMatrix() -> float4x4 {
        var matrix = float4x4()
        
        matrix.translate(positionX, y: positionY, z: positionZ)
        matrix.rotateAroundX(rotationX, y: rotationY, z: rotationZ)
        matrix.scale(scale, y: scale, z: scale)
        
        return matrix
    }
}
