//
//  BufferProvider.swift
//  mBall
//
//  Created by Dominik Plšek on 27/11/2016.
//  Copyright © 2016 mBall Team. All rights reserved.
//

import MetalKit

class BufferProvider {
    static var shared: BufferProvider!
    
    let inflightBuffersCount: Int = 30
    
    private var uniformsBuffers: [MTLBuffer] = []
    private var bufferIndex: Int = 0
    
    var availableResourcesSemaphore: DispatchSemaphore

    init(device: MTLDevice) {
        availableResourcesSemaphore = DispatchSemaphore(value: inflightBuffersCount)
        for _ in 0..<inflightBuffersCount {
            let buffer = device.makeBuffer(length: MemoryLayout<Uniforms>.size, options: .cpuCacheModeWriteCombined)
            uniformsBuffers.append(buffer)
        }
    }
    
    func nextUniformsBuffer(modelMatrix: float4x4, modelViewMatrix: float4x4, normalMatrix: float4x4, modelViewProjectionMatrix: float4x4) -> MTLBuffer {
        let buffer = uniformsBuffers[bufferIndex]
        let bufferPtr = UnsafeMutablePointer<Uniforms>(OpaquePointer(buffer.contents()))
        
        var uniforms = bufferPtr.pointee
        
        uniforms.modelMatrix = modelMatrix
        uniforms.modelViewMatrix = modelViewMatrix
        uniforms.normalMatrix = normalMatrix
        uniforms.modelViewProjectionMatrix = modelViewProjectionMatrix
        
        bufferPtr.pointee = uniforms
        
        bufferIndex = (bufferIndex + 1) % inflightBuffersCount

        return buffer
    }
    
    deinit {
        for _ in 0..<inflightBuffersCount {
            availableResourcesSemaphore.signal()
        }
    }
}
