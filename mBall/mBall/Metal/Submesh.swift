//
//  Submesh.swift
//  mBall
//
//  Created by Dominik Plšek on 24/11/2016.
//  Copyright © 2016 mBall Team. All rights reserved.
//
import Foundation
import MetalKit


class Submesh {
    var materialUniformsBuffer: MTLBuffer
    var diffuseTexture: MTLTexture?
    var submesh: MTKSubmesh
    
    init(submesh: MTKSubmesh, mdlSubmesh: MDLSubmesh?, device: MTLDevice) {
        self.submesh = submesh
        
        materialUniformsBuffer = device.makeBuffer(length: MemoryLayout<MaterialUniforms>.size, options: .cpuCacheModeWriteCombined)
        
        // #@!#@!#@!#@! hack to convert raw pointer to memory to Swift struct...
        let materialUniformsPtr = UnsafeMutablePointer<MaterialUniforms>(OpaquePointer(materialUniformsBuffer.contents()))
        var materialUniforms = materialUniformsPtr.pointee
        
        if let material = mdlSubmesh?.material {
            if let property = material.propertyNamed("baseColor") {
                if property.type == MDLMaterialPropertyType.string,
                    let fileString = property.stringValue {
                    let fileLocation = "file://\(fileString)"
                    if let textureURL = URL(string: fileLocation) {
                        guard let textureData = try? Data(contentsOf: textureURL) else { fatalError() }
                        let textureLoader = MTKTextureLoader(device: device)
                        do {
                            diffuseTexture = try textureLoader.newTexture(with: textureData,
                                                                          options: [MTKTextureLoaderOptionSRGB : (false as NSNumber)])
                        } catch {
                            fatalError("Creating texture in submesh failed")
                        }
                    }
                }
            }
            if let property = material.propertyNamed("specular") {
                if property.type == MDLMaterialPropertyType.float4 {
                    materialUniforms.specularColor = property.float4Value
                } else if property.type == MDLMaterialPropertyType.float3 {
                    let color3 = property.float3Value
                    materialUniforms.specularColor = float4(color3.x, color3.y, color3.z, 1.0)
                }
            }
            if let property = material.propertyNamed("specularExponent") {
                materialUniforms.specularIntensity = property.floatValue
            }
            if let property = material.propertyNamed("emission") {
                if property.type == MDLMaterialPropertyType.float4 {
                    materialUniforms.emissiveColor = property.float4Value
                } else if property.type == MDLMaterialPropertyType.float3 {
                    let color3 = property.float3Value
                    materialUniforms.emissiveColor = float4(color3.x, color3.y, color3.z, 1.0)
                }
            }
        }
        
        materialUniformsPtr.pointee = materialUniforms
    }
    
    func render(encoder: MTLRenderCommandEncoder) {
        if diffuseTexture != nil {
            encoder.setFragmentTexture(diffuseTexture, at: TextureIndex.DiffuseTextureIndex.hashValue)
        }
        
        encoder.setFragmentBuffer(materialUniformsBuffer, offset: 0, at: 0)
    
        encoder.drawIndexedPrimitives(type: submesh.primitiveType, indexCount: submesh.indexCount, indexType: submesh.indexType, indexBuffer: submesh.indexBuffer.buffer, indexBufferOffset: submesh.indexBuffer.offset)
    }
}
