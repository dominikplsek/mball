//
//  Renderer.swift
//  mBall
//
//  Created by Dominik Plšek on 26/11/2016.
//  Copyright © 2016 mBall Team. All rights reserved.
//

import MetalKit

class Renderer {
    // MetalKit View
    private var mtkView: MTKView!
    
    // Metal
    private var device: MTLDevice! = nil
    private var pipelineState: MTLRenderPipelineState! = nil
    private var commandQueue: MTLCommandQueue! = nil
    private var defaultLibrary: MTLLibrary! = nil
    private var depthStencilState: MTLDepthStencilState! = nil
    
    // Model I/O
    lazy var bufferAllocator: MTKMeshBufferAllocator = MTKMeshBufferAllocator(device: self.device)
    private var mdlVertexDescriptor: MDLVertexDescriptor! = nil
    
    private var bufferProvider: BufferProvider! = nil
    
    // Camera
    private var projectionMatrix: float4x4 = float4x4(matrix_identity_float4x4)
    private var viewMatrix: float4x4 = float4x4(matrix_identity_float4x4)
    private var modelMatrix: float4x4 = float4x4(matrix_identity_float4x4)
    private var modelViewMatrix: float4x4 = float4x4(matrix_identity_float4x4)
    
    // Main game character
    var ball: Ball! = nil
    
    var renderObjects: [RenderObject] = []
    
    init(view: MTKView) {
        self.mtkView = view
        self.setupMetal()
        self.setupView()
        self.setupPipeline()
        self.setupBufferProvider()
        self.loadResources()
        self.reshape()
    }
    
    private func setupBufferProvider() {
        let bufferProvider = BufferProvider(device: self.device)
        BufferProvider.shared = bufferProvider
        self.bufferProvider = bufferProvider
    }
    
    private func setupMetal() {
        device = MTLCreateSystemDefaultDevice()
    }
    
    private func setupView() {
        mtkView.device = device
    }
    
    // MARK: - Metal Pipeline
    private func setupPipeline() {
        commandQueue = device.makeCommandQueue()
        
        defaultLibrary = device.newDefaultLibrary()
        
        let mtlVertexDescriptor = MTLVertexDescriptor()
        
        // Positions
        mtlVertexDescriptor.attributes[VertexAttributes.VertexAttributePosition.hashValue].format = .float3
        mtlVertexDescriptor.attributes[VertexAttributes.VertexAttributePosition.hashValue].offset = 0
        mtlVertexDescriptor.attributes[VertexAttributes.VertexAttributePosition.hashValue].bufferIndex = BufferIndex.MeshVertexBuffer.hashValue
        // Normals
        mtlVertexDescriptor.attributes[VertexAttributes.VertexAttributeNormal.hashValue].format = .float3
        mtlVertexDescriptor.attributes[VertexAttributes.VertexAttributeNormal.hashValue].offset = MemoryLayout<float3>.size
        mtlVertexDescriptor.attributes[VertexAttributes.VertexAttributeNormal.hashValue].bufferIndex = BufferIndex.MeshVertexBuffer.hashValue
        // Texture coordinates
        mtlVertexDescriptor.attributes[VertexAttributes.VertexAttributeTexcoord.hashValue].format = .float2
        mtlVertexDescriptor.attributes[VertexAttributes.VertexAttributeTexcoord.hashValue].offset = MemoryLayout<float3>.size*2
        mtlVertexDescriptor.attributes[VertexAttributes.VertexAttributeTexcoord.hashValue].bufferIndex = BufferIndex.MeshVertexBuffer.hashValue
        // Single interleaved buffer
        mtlVertexDescriptor.layouts[BufferIndex.MeshVertexBuffer.hashValue].stride = MemoryLayout<float3>.size*2+MemoryLayout<float2>.size
        mtlVertexDescriptor.layouts[BufferIndex.MeshVertexBuffer.hashValue].stepRate = 1
        mtlVertexDescriptor.layouts[BufferIndex.MeshVertexBuffer.hashValue].stepFunction = .perVertex
        
        // MARK: - Pipeline setup
        let pipelineStateDescriptor = MTLRenderPipelineDescriptor()
        pipelineStateDescriptor.vertexFunction = defaultLibrary!.makeFunction(name: "vertex_program")
        pipelineStateDescriptor.fragmentFunction = defaultLibrary!.makeFunction(name: "fragment_program")
        pipelineStateDescriptor.vertexDescriptor = mtlVertexDescriptor
        pipelineStateDescriptor.sampleCount = mtkView.sampleCount
        pipelineStateDescriptor.colorAttachments[0].pixelFormat = mtkView.colorPixelFormat
        pipelineStateDescriptor.depthAttachmentPixelFormat = mtkView.depthStencilPixelFormat
        pipelineStateDescriptor.stencilAttachmentPixelFormat = mtkView.depthStencilPixelFormat
        
        do {
            pipelineState = try device.makeRenderPipelineState(descriptor: pipelineStateDescriptor)
        } catch {
            fatalError("Cannot make render pipeline state")
        }
        
        // MARK: - Depth
        let depthStencilDescriptor = MTLDepthStencilDescriptor()
        depthStencilDescriptor.depthCompareFunction = .less
        depthStencilDescriptor.isDepthWriteEnabled = true
        depthStencilState = device.makeDepthStencilState(descriptor: depthStencilDescriptor)
        
        setupModelIO(mtlVertexDescriptor: mtlVertexDescriptor)
    }
    
    // MARK: - ModelIO
    private func setupModelIO(mtlVertexDescriptor: MTLVertexDescriptor) {
        mdlVertexDescriptor = MTKModelIOVertexDescriptorFromMetal(mtlVertexDescriptor)
        // Positions
        let vertexPositionAttr = mdlVertexDescriptor.attributes[VertexAttributes.VertexAttributePosition.hashValue] as! MDLVertexAttribute
        vertexPositionAttr.name = MDLVertexAttributePosition
        mdlVertexDescriptor.attributes[VertexAttributes.VertexAttributePosition.hashValue] = vertexPositionAttr
        // Normals
        let vertexNormalAttr = mdlVertexDescriptor.attributes[VertexAttributes.VertexAttributeNormal.hashValue] as! MDLVertexAttribute
        vertexNormalAttr.name = MDLVertexAttributeNormal
        mdlVertexDescriptor.attributes[VertexAttributes.VertexAttributeNormal.hashValue] = vertexNormalAttr
        // Texture Coordinates
        let vertexTexCoordAttr = mdlVertexDescriptor.attributes[VertexAttributes.VertexAttributeTexcoord.hashValue] as! MDLVertexAttribute
        vertexTexCoordAttr.name = MDLVertexAttributeTextureCoordinate
        mdlVertexDescriptor.attributes[VertexAttributes.VertexAttributeTexcoord.hashValue] = vertexTexCoordAttr
    }
    
    private func loadResources() {
        
        let floor = Floor(name: "Floor", meshes:loadMeshes(resource: "floor"))
        self.ball = Ball(name: "Ball", meshes: loadMeshes(resource: "basic-ball"))
        
        renderObjects.append(floor)
        renderObjects.append(ball)
        
        for _ in 0..<Game.shared.numberOfCoins {
            let coin = Coin(name: "Coin", meshes: loadMeshes(resource: "coin"))
            renderObjects.append(coin)
        }
        for _ in 0..<Game.shared.numberOfTraps {
            let trap = Trap(name: "Trap", meshes: loadMeshes(resource: "trap"))
            renderObjects.append(trap)
        }
    }
    
    // MARK: - Meshes loading
    func loadMeshes(resource: String) -> [Mesh] {
        var meshes: [Mesh] = []
        
        guard let resourcePath = Bundle.main.path(forResource: resource, ofType: "obj") else {
            fatalError("Can't locate OBJ file: \(resource).")
        }
        
        guard let resourceURL = URL(string: resourcePath) else {
            fatalError("Cannot create url from obj filepath: \(resourcePath)")
        }
        
        let mdlAsset = MDLAsset(url: resourceURL, vertexDescriptor: mdlVertexDescriptor,
                                bufferAllocator: bufferAllocator)
        
        let mtkMeshes: [MTKMesh]?
        var mdlMeshes: NSArray?
        
        do {
            mtkMeshes = try MTKMesh.newMeshes(from: mdlAsset, device: device, sourceMeshes: &mdlMeshes)
        } catch {
            fatalError("Cannot create ball meshes")
        }
        
        for index in 0..<mtkMeshes!.count {
            let mesh = Mesh(mtkMesh: mtkMeshes![index], mdlMesh: mdlMeshes![index] as! MDLMesh, device: device)
            meshes.append(mesh)
        }
        
        return meshes
    }
    
    private func collision(ballPosition: CGVector, objectPosition: CGVector) -> Bool
    {
        if (abs(ballPosition.dx + objectPosition.dx) < 0.13 &&
            abs(ballPosition.dy - objectPosition.dy) < 0.13)   {
            return  ( ballPosition.dy != 0.0 && ballPosition.dy != 0.0 )
        }
        return false
    }
    
    
    private func updateRenderObjectsUniforms() {
        
        var ballPosition = CGVector(dx: 0, dy: 0)
        var coinPosition = CGVector(dx: 0, dy: 0)
        var trapPosition = CGVector(dx: 0, dy: 0)
        var i = 0
        var indexToRemove: Int = 0
        var remove: Bool = false
        for renderObject in renderObjects {
            renderObject.update(viewMatrix: viewMatrix, projectionMatrix: projectionMatrix)
            if renderObject is Ball {
                ballPosition = CGVector(dx: CGFloat(renderObject.positionX), dy: CGFloat(renderObject.positionZ))
            }
            if renderObject is Coin {
                coinPosition = CGVector(dx: CGFloat(renderObject.positionX), dy: CGFloat(renderObject.positionZ))
                if(collision(ballPosition: ballPosition, objectPosition: coinPosition)) {
                    indexToRemove = i
                    remove = true
                    Game.shared.collectedCoins += 1
                }
            }
            if renderObject is Trap {
                trapPosition = CGVector(dx: CGFloat(renderObject.positionX), dy: CGFloat(renderObject.positionZ))
                if(collision(ballPosition: ballPosition, objectPosition: trapPosition)) {
                    indexToRemove = i
                    remove = true
                    Game.shared.activateTrap()
                }
            }
            i += 1
        }
        if(remove)  {
            renderObjects.remove(at: indexToRemove)
            remove = false
        }
        
    }
    
    private func updateSharedUniforms() {
        viewMatrix.translateToCenter()
        
        if ball != nil {
            viewMatrix.rotateAroundX(0.0, y: self.ball.ballDirection, z: 0.0)
            viewMatrix.translate(self.ball.positionX, y:-self.ball.positionY, z:-self.ball.positionZ)
        }

    }
    
    private func updateUniforms() {
        updateRenderObjectsUniforms()
        updateSharedUniforms()
    }
    
    func reshape() {
        projectionMatrix = float4x4.makePerspectiveViewAngle(float4x4.degrees(toRad: 65.0),
                                                             aspectRatio: Float(mtkView.bounds.size.width / mtkView.bounds.size.height),
                                                             nearZ: 0.01, farZ: 100.0)
        viewMatrix = float4x4(matrix_identity_float4x4)
        viewMatrix.translate(0.0, y:0.0, z: 0.0)
    }
    
    // MARK: - Render
    func render(view: MTKView) {
        updateUniforms()
        
        guard let drawable = view.currentDrawable else {
            print("Can't obtain next drawable.")
            return
        }
        
        _ = bufferProvider.availableResourcesSemaphore.wait(timeout: DispatchTime.distantFuture)
        
        let commandBuffer = commandQueue.makeCommandBuffer()
        
        guard let renderPassDescriptor = view.currentRenderPassDescriptor else {
            print("Can't obtain render pass descriptor from view.")
            return
        }

        renderPassDescriptor.colorAttachments[0].loadAction = .clear
        renderPassDescriptor.colorAttachments[0].clearColor = MTLClearColor(red: 0.2, green: 0.5,
                                                                            blue: 0.95, alpha: 1.0)
        //renderPassDescriptor.colorAttachments[0].clearColor = MTLClearColor(red: 0.0, green: 0.0,
//                                                                            blue: 0.0, alpha: 1.0)
        
        let renderEncoder = commandBuffer.makeRenderCommandEncoder(descriptor: renderPassDescriptor)
        renderEncoder.setViewport(MTLViewport(originX: 0, originY: 0, width: Double(mtkView.drawableSize.width), height: Double(mtkView.drawableSize.height), znear: 0, zfar: 1))
        renderEncoder.setDepthStencilState(depthStencilState)
        renderEncoder.setRenderPipelineState(pipelineState)
        
        for renderObject in renderObjects {
            renderObject.render(encoder: renderEncoder)
        }
        
        renderEncoder.endEncoding()
        
        commandBuffer.addCompletedHandler { [weak self] (commandBuffer) in
            self?.bufferProvider.availableResourcesSemaphore.signal()
        }
        
        commandBuffer.present(drawable)
        commandBuffer.commit()
    }
}
