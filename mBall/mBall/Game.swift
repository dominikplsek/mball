//
//  Game.swift
//  mBall
//
//  Created by Dominik Plšek on 30/11/2016.
//  Copyright © 2016 mBall Team. All rights reserved.
//

import Foundation

protocol GameEvents: class {
    func playerDidActivateTrap()
}

class Game {
    
    static var shared: Game!
    
    // Number of coins in game
    let numberOfCoins = 5
    // Number of traps in game
    let numberOfTraps = 4
    // Game time in seconds
    var time: Int = 0
    // Check coins
    var allCoinsCollected: (() -> Void)?

    weak var delegate: GameEvents?
    
    // Collected coins by player
    var collectedCoins = 0  {
        didSet  {
            allCoinsCollected?()
        }
    }
    
    let entityManager: EntityManager
    
    init() {
        entityManager = EntityManager()
        EntityManager.shared = entityManager
    }
    
    func activateTrap() {
        delegate?.playerDidActivateTrap()
    }
    
}
