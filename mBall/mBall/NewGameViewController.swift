//
//  NewGameViewController.swift
//  mBall
//
//  Created by Jiri Skotak on 01.12.16.
//  Copyright © 2016 mBall Team. All rights reserved.
//

import Foundation
import UIKit

class NewGameViewController: UIViewController {
    
    @IBOutlet weak var playerLoseLabel: UILabel!
    @IBOutlet weak var coinsCollectedLabel: UILabel!
    @IBOutlet weak var inTimeLabel: UILabel!
    var coinsCollected: Int = 0
    var inTime: Int = 0
    var playerLose = false
    
    override func viewWillAppear(_ animated: Bool) {
        if playerLose {
            playerLoseLabel.isHidden = false
            coinsCollectedLabel.isHidden = true
            inTimeLabel.isHidden = true
        }
        else {
            coinsCollectedLabel.isHidden = false
            inTimeLabel.isHidden = false
            coinsCollectedLabel.text = "You collected : \(coinsCollected) coins"
            inTimeLabel.text = "In time: \(inTime) sec"
        }
        
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ShowGameView" {
            playerLose = false
            playerLoseLabel.isHidden = true
        }
    }
    
}
